ARDYsynth is a project to develop an Arduino Nano based synthesizer
that can be played with MIDI controllers, is battery powered, has
polyphonic oscillators, ADSR volume and filter control, and effects.
 
For project details please see the wiki
https://gitlab.com/efarmiga/ardysynth/-/wikis/Ardysynth-Overview
<!---
efarmiga/efarmiga is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
